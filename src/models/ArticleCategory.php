<?php

namespace werewolf8904\cmscontent\models;

use werewolf8904\cmscontent\models\query\ArticleCategoryQuery;
use werewolf8904\cmscore\behaviors\CacheInvalidateBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "article category".
 *
 * @property integer $id
 * @property string $view
 * @property string $view_item
 * @property string $slug
 * @property integer $parent_id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property string $pathString
 *
 * @property Article[] $articles
 * @property ArticleCategory $parent
 * @property ArticleCategoryI18n $articleCategoryI18n
 * @property ArticleCategoryI18n[] $articleCategoryI18ns
 *
 */
class ArticleCategory extends ActiveRecord
{
    public const STATUS_PUBLISHED = 1;
    public const STATUS_DRAFT = 0;

    /**
     * @var string
     */
    public $tmpl_path = '@werewolf8904/cmscontent/frontend/views/article/category-tmpl';

    /**
     * @var string
     */
    public $default_view = 'default';

    /**
     * @var string
     */
    public $tmpl_path_item = '@werewolf8904/cmscontent/frontend/views/article/category-tmpl/_items';
    /**
     * @var string
     */
    public $default_item = 'default-item';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article_category}}';
    }

    /**
     * @return ArticleCategoryQuery
     */
    public static function find()
    {
        return new ArticleCategoryQuery(static::class);
    }

    /**
     * @param array $row
     *
     * @return object|\werewolf8904\cmscontent\models\ArticleCategory|\yii\db\ActiveRecord
     * @throws \yii\base\InvalidConfigException
     */
    public static function instantiate($row)
    {
        return Yii::createObject(static::class);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'cacheInvalidate' => [
                'class' => CacheInvalidateBehavior::class,
                'tags' => [self::class,],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['view', 'view_item',], 'required',],
            [['slug',], 'required', 'enableClientValidation' => false,],
            [['status',], 'boolean',],
            [['sort',], 'integer',],
            [['sort', 'status',], 'default', 'value' => 0,],
            [
                ['parent_id',],
                'exist',
                'isEmpty' => function ($v) {
                    return !(bool)$v;
                },
                'skipOnError' => true,
                'skipOnEmpty' => true,
                'targetClass' => static::class,
                'targetAttribute' => ['parent_id' => 'id',],
            ],
            [['view', 'view_item', 'slug',], 'string', 'max' => 255,],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('content/model_labels', 'ID'),
            'status' => Yii::t('content/model_labels', 'Status'),
            'sort' => Yii::t('content/model_labels', 'Sort'),
            'parent_id' => Yii::t('content/model_labels', 'Article Category'),
            'view' => Yii::t('content/model_labels', 'View'),
            'view_item' => Yii::t('content/model_labels', 'View Item'),
            'slug' => Yii::t('content/model_labels', 'Slug'),
            'created_at' => Yii::t('content/model_labels', 'Created At'),
            'updated_at' => Yii::t('content/model_labels', 'Updated At'),
        ];
    }

    /**
     * @param string $tmpl_cat
     *
     * @return array
     * @throws \yii\base\InvalidArgumentException
     */
    public function getTemplates($tmpl_cat = 'tmpl_path')
    {
        return \code2magic\core\helpers\TemplateHelper::getTemplates($this->$tmpl_cat);
    }

    /**
     * @return string
     */
    public function getMainView()
    {
        return $this->tmpl_path . '/' . ($this->view ?? $this->default_view);
    }

    /**
     * @return string
     */
    public function getItemView()
    {
        return $this->tmpl_path_item . '/' . ($this->view_item ?? $this->default_item);
    }

    /**
     * @return string
     */
    public function getPathString()
    {
        $arr[] = $this->slug;
        $this->getParentSlug($this->parent, $arr);
        return implode('/', $arr);
    }

    /**
     * @param $model
     * @param $arr
     */
    protected function getParentSlug($model, &$arr): void
    {
        if ($model && isset($model['slug'])) {
            $arr[] = $model->slug;
            if ($model->parent) {
                $this->getParentSlug($model, $arr);
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::class, ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasMany(static::class, ['id' => 'parent_id']);
    }

    /**
     * @param null $language_code
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArticleCategoryI18n($language_code = null)
    {
        $language_code = $language_code ?: Yii::$app->language;
        $query = $this->getArticleCategoryI18ns()->andWhere([ArticleCategoryI18n::tableName() . '.language_code' => $language_code]);
        $query->multiple = false;
        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleCategoryI18ns()
    {
        return $this->hasMany(ArticleCategoryI18n::class, ['article_category_id' => 'id']);
    }
}
