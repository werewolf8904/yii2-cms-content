<?php

namespace werewolf8904\cmscontent\models;

use cheatsheet\Time;
use werewolf8904\cmscontent\models\query\ArticleQuery;
use werewolf8904\cmscore\behaviors\CacheInvalidateBehavior;
use werewolf8904\cmscore\behaviors\TimeBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property string $thumbnail
 * @property boolean $use_short_description
 * @property integer $article_category_id
 * @property integer $status
 * @property string $view
 * @property string $slug
 * @property integer $published_at
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ArticleCategory $articleCategory
 * @property ArticleAttachment[] $articleAttachments
 * @property ArticleI18n $articleI18ns
 *
 * @property string $publishedDate
 */
class Article extends ActiveRecord
{
    public const STATUS_PUBLISHED = 1;
    public const STATUS_DRAFT = 0;

    /**
     * @var string
     */
    public $tmpl_path = '@werewolf8904/cmscontent/frontend/views/article/article-tmpl';

    /**
     * @param array $row
     *
     * @return object|\werewolf8904\cmscontent\models\Article|\yii\db\ActiveRecord
     * @throws \yii\base\InvalidConfigException
     */
    public static function instantiate($row)
    {
        return Yii::createObject(static::class);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article}}';
    }

    /**
     * @return ArticleQuery
     */
    public static function find()
    {
        return new ArticleQuery(static::class);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'cacheInvalidate' => [
                'class' => CacheInvalidateBehavior::class,
                'tags' => [self::class,],
            ],
            'time' => [
                'class' => TimeBehavior::class,
                'fields' => ['published_at', 'end_at',]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['published_at',], 'default', 'value' => time(),],
            [['end_at',], 'default', 'value' => $this->timeplus100years(),],
            [['view',], 'default', 'value' => 'view',],
            [['view',], 'required',],
            [['slug',], 'required', 'enableClientValidation' => false,],
            [['status',], 'boolean',],
            [['use_short_description',], 'boolean',],
            [['article_category_id',], 'integer',],
            [['use_short_description', 'status',], 'default', 'value' => 0,],
            [['thumbnail',], 'string', 'max' => 1024,],
            [['view', 'slug',], 'string', 'max' => 255,],
            [['article_category_id',], 'exist', 'skipOnEmpty' => true, 'targetRelation' => 'articleCategory',],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('content/model_labels', 'ID'),
            'name' => Yii::t('content/model_labels', 'Name'),
            'content' => Yii::t('content/model_labels', 'Content'),
            'slug' => Yii::t('content/model_labels', 'Slug'),
            'article_category_id' => Yii::t('content/model_labels', 'Article Category'),
            'use_short_description' => Yii::t('content/model_labels', 'Use Short Description'),
            'short_description' => Yii::t('content/model_labels', 'Short Description'),
            'status' => Yii::t('content/model_labels', 'Status'),
            'thumbnail' => Yii::t('content/model_labels', 'Thumbnail'),
            'created_at' => Yii::t('content/model_labels', 'Created At'),
            'published_at' => Yii::t('content/model_labels', 'Published At'),
            'updated_at' => Yii::t('content/model_labels', 'Updated At'),
            'end_at' => Yii::t('content/model_labels', 'End At'),
            'view' => Yii::t('content/model_labels', 'View'),
        ];
    }

    public function timeplus100years()
    {
        return time() + (10 * Time::SECONDS_IN_A_YEAR);
    }

    /**
     * @return array
     * @throws \yii\base\InvalidArgumentException
     */
    public function getTemplates()
    {
        return \code2magic\core\helpers\TemplateHelper::getTemplates($this->tmpl_path);
    }

    /**
     * @return string
     */
    public function getViewPath()
    {
        return $this->tmpl_path . '/' . $this->view;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidArgumentException
     * @throws \yii\base\InvalidConfigException
     */
    public function getPublishedDate()
    {
        return Yii::$app->formatter->asDate($this->published_at);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleCategory()
    {
        return $this->hasOne(ArticleCategory::class, ['id' => 'article_category_id',]);
    }

    /**
     * @param null $language_code
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArticleI18n($language_code = null)
    {
        $language_code = $language_code ?: Yii::$app->language;
        $query = $this->getArticleI18ns()->andWhere([ArticleI18n::tableName() . '.language_code' => $language_code,]);
        $query->multiple = false;
        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleI18ns()
    {
        return $this->hasMany(ArticleI18n::class, ['article_id' => 'id',]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleAttachments()
    {
        return $this->hasMany(ArticleAttachment::class, ['article_id' => 'id',]);
    }
}
