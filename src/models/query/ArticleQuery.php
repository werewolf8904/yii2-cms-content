<?php

namespace werewolf8904\cmscontent\models\query;

use werewolf8904\cmscontent\models\Article;
use yii\db\ActiveQuery;

/**
 * Class ArticleQuery
 *
 * @package common\models\articles\query
 */
class ArticleQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function published()
    {
        $this->andWhere([Article::tableName() . '.status' => Article::STATUS_PUBLISHED,]);
        $this->andWhere(['<', Article::tableName() . '.published_at', time(),]);
        $this->andWhere(['>', Article::tableName() . '.end_at', time(),]);
        return $this;
    }

    /**
     * 
     */
    public function in_categories($id)
    {
        return $this->andWhere([Article::tableName() . '.article_category_id' => $id,]);
    }
}
