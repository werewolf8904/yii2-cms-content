<?php

namespace werewolf8904\cmscontent\models\query;

use werewolf8904\cmscontent\models\ArticleCategory;
use yii\db\ActiveQuery;

/**
 * Class ArticleCategoryQuery
 *
 * @package common\models\articles\query
 * @see     ArticleCategory::find()
 */
class ArticleCategoryQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function published(): self
    {
        $this->andWhere([$this->modelClass::tableName() . '.status' => $this->modelClass::STATUS_PUBLISHED,]);
        return $this;
    }
}
