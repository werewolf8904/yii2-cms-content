<?php

namespace werewolf8904\cmscontent\models\query;

use werewolf8904\cmscontent\models\Page;
use yii\db\ActiveQuery;

/**
 * Class PageQuery
 *
 * @package common\models\query
 * @see     $modelClass Page
 */
class PageQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function published()
    {
        $this->andWhere([Page::tableName() . '.status' => Page::STATUS_PUBLISHED,]);
        return $this;
    }
}
