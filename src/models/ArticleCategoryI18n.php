<?php

namespace werewolf8904\cmscontent\models;

use werewolf8904\cmscore\models\Language;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%article_i18n}}".
 *
 * @property integer $id
 * @property string $language_code
 * @property integer $article_category_id
 * @property string $name
 * @property string $content
 * @property string $seo_title
 * @property string $seo_h1
 * @property string $seo_description
 *
 * @property ArticleCategory $articleCategory
 * @property Language $languageCode
 */
class ArticleCategoryI18n extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article_category_i18n}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language_code', 'article_category_id', 'name',], 'required',],
            [['name', 'content', 'seo_title', 'seo_h1', 'seo_description',], 'default', 'value' => '',],
            [['article_category_id',], 'integer',],
            [['content',], 'string'],
            [['seo_description',], 'string', 'max' => 180,],
            [['seo_title',], 'string', 'max' => 80,],
            [['name', 'seo_h1',], 'string', 'max' => 255,],
            [['content', 'seo_title', 'seo_h1', 'seo_description',], 'default', 'value' => '',],
            [['language_code',], 'string', 'max' => 6,],
            [
                ['article_category_id', 'language_code',],
                'unique',
                'targetAttribute' => ['article_category_id', 'language_code',],
            ],
            [
                ['article_category_id',],
                'exist', 'skipOnEmpty' => false,
                'targetRelation' => 'articleCategory',
            ],
            [
                ['language_code',],
                'exist',
                'skipOnEmpty' => false,
                'targetClass' => Language::class,
                'targetAttribute' => ['language_code' => 'code',],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('content/model_labels', 'ID'),
            'language_code' => Yii::t('content/model_labels', 'Language Code'),
            'article_category_id' => Yii::t('content/model_labels', 'Article ID'),
            'name' => Yii::t('content/model_labels', 'Name'),
            'content' => Yii::t('content/model_labels', 'Content'),
            'seo_title' => Yii::t('content/model_labels', 'Seo Title'),
            'seo_h1' => Yii::t('content/model_labels', 'Seo H1'),
            'seo_description' => Yii::t('content/model_labels', 'Seo Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleCategory()
    {
        return $this->hasOne(ArticleCategory::class, ['id' => 'article_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguageCode()
    {
        return $this->hasOne(Language::class, ['code' => 'language_code']);
    }
}
