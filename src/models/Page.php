<?php

namespace werewolf8904\cmscontent\models;

use werewolf8904\cmscontent\models\query\PageQuery;
use werewolf8904\cmscore\behaviors\CacheInvalidateBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%page}}".
 *
 * @property integer $id
 * @property integer $status
 * @property string $slug
 * @property string $view
 * @property integer $created_at
 * @property integer $updated_at
 *
 *
 * @property PageI18n[] $pageI18ns
 */
class Page extends ActiveRecord
{
    public const STATUS_DRAFT = 0;
    public const STATUS_PUBLISHED = 1;

    public $tmpl_path = '@werewolf8904/cmscontent/frontend/views/page';

    public static function instantiate($row)
    {
        return Yii::createObject(static::class);
    }

    /**
     * @return array
     * @throws \yii\base\InvalidArgumentException
     */
    public function getTemplates()
    {
        return \code2magic\core\helpers\TemplateHelper::getTemplates($this->tmpl_path);
    }

    /**
     * @return string
     */
    public function getViewPath()
    {
        return $this->tmpl_path . '/' . $this->view;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'cacheInvalidate' => [
                'class' => CacheInvalidateBehavior::class,
                'tags' => [
                    self::class,
                ],
            ],
        ];
    }

    /**
     * @return PageQuery
     */
    public static function find()
    {
        return new PageQuery(static::class);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status',], 'required',],
            [['status', 'created_at', 'updated_at',], 'integer',],
            [['slug',], 'string', 'max' => 1024,],
            [['view',], 'string', 'max' => 255,],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('content/model_labels', 'ID'),
            'status' => Yii::t('content/model_labels', 'Status'),
            'view' => Yii::t('content/model_labels', 'View'),
            'created_at' => Yii::t('content/model_labels', 'Created At'),
            'updated_at' => Yii::t('content/model_labels', 'Updated At'),
            'slug' => Yii::t('content/model_labels', 'Slug'),
        ];
    }

    /**
     * @param null $language_code
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPageI18n($language_code = null)
    {
        $language_code = $language_code ?: Yii::$app->language;
        $query = $this->getPageI18ns()->andWhere([PageI18n::tableName() . '.language_code' => $language_code,]);
        $query->multiple = false;
        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageI18ns()
    {
        return $this->hasMany(PageI18n::class, ['page_id' => 'id',])->indexBy('language_code');
    }
}
