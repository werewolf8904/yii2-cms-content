<?php

namespace werewolf8904\cmscontent\models;

use werewolf8904\cmscore\models\Language;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%page_i18n}}".
 *
 * @property integer $id
 * @property string $language_code
 * @property integer $page_id
 * @property string $name
 * @property string $content
 * @property string $seo_title
 * @property string $seo_h1
 * @property string $seo_description
 *
 * @property Language $language
 * @property Page $page
 */
class PageI18n extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_i18n}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language_code', 'page_id', 'name', 'content',], 'required',],
            [['name', 'content', 'seo_title', 'seo_h1', 'seo_description',], 'default', 'value' => '',],
            [['page_id',], 'integer',],
            [['content', 'seo_description',], 'string',],
            [['name', 'seo_title', 'seo_h1',], 'string', 'max' => 255,],
            [['language_code',], 'string', 'max' => 6,],
            [
                ['language_code',],
                'exist', 'skipOnError' => true,
                'targetClass' => Language::class,
                'targetAttribute' => ['language_code' => 'code',],
            ],
            [
                ['page_id',],
                'exist',
                'skipOnError' => true,
                'targetClass' => Page::class,
                'targetAttribute' => ['page_id' => 'id',],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('content/model_labels', 'ID'),
            'language_code' => Yii::t('content/model_labels', 'Language'),
            'page_id' => Yii::t('content/model_labels', 'Page ID'),
            'name' => Yii::t('content/model_labels', 'Name'),
            'content' => Yii::t('content/model_labels', 'Content'),
            'seo_title' => Yii::t('content/model_labels', 'Seo Title'),
            'seo_h1' => Yii::t('content/model_labels', 'Seo H1'),
            'seo_description' => Yii::t('content/model_labels', 'Seo Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::class, ['code' => 'language_code',]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::class, ['id' => 'page_id',]);
    }
}
