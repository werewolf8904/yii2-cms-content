<?php

namespace werewolf8904\cmscontent\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%article_attachment}}".
 *
 * @property integer $id
 * @property integer $article_id
 * @property string  $path
 * @property string  $base_url
 * @property string  $type
 * @property string  $size
 * @property string  $name
 * @property int     $order      [int(11)]
 * @property int     $created_at [int(11)]
 * 
 * @property string  $url
 *
 * @property Article $article
 */
class ArticleAttachment extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article_attachment}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_id', 'path',], 'required',],
            [['article_id', 'size',], 'integer',],
            [['base_url', 'path', 'type', 'name',], 'string', 'max' => 255,],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('content/model_labels', 'ID'),
            'article_id' => Yii::t('content/model_labels', 'Article ID'),
            'base_url' => Yii::t('content/model_labels', 'Base Url'),
            'path' => Yii::t('content/model_labels', 'Path'),
            'size' => Yii::t('content/model_labels', 'Size'),
            'type' => Yii::t('content/model_labels', 'Type'),
            'name' => Yii::t('content/model_labels', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::class, ['id' => 'article_id',]);
    }


    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->base_url . '/' . $this->path;
    }
}
