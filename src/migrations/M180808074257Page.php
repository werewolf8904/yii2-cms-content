<?php

namespace werewolf8904\cmscontent\migrations;

use werewolf8904\cmscore\db\Migration;

/**
 * Class M180808074257Page
 */
class M180808074257Page extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = $this->tableOptions;

        $this->createTable('{{%page}}', [
            'id' => $this->primaryKey(),
            'status' => $this->tinyInteger(1)->notNull()->defaultValue(0),
            'slug' => $this->string(255)->notNull()->defaultValue(''),
            'view' => $this->string(255)->notNull(),
            'data' => $this->text(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%page_i18n}}', [
            'id' => $this->primaryKey(),
            'language_code' => $this->string(6)->notNull(),
            'page_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'content' => $this->text(),
            'seo_title' => $this->string(80)->notNull()->defaultValue(''),
            'seo_h1' => $this->string()->notNull()->defaultValue(''),
            'seo_description' => $this->string(180)->notNull()->defaultValue(''),
        ], $tableOptions);
        $this->createIndex('p18n_lang_f_id', '{{%page_i18n}}', ['page_id', 'language_code',], true);
        $this->addForeignKey('fk_page_i18n_2_page',
            '{{%page_i18n}}', 'page_id',
            '{{%page}}', 'id',
            'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_page_i18n_2_language',
            '{{%page_i18n}}', 'language_code',
            '{{%language}}', 'code',
            'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%page_i18n}}');
        $this->dropTable('{{%page}}');
    }
}
