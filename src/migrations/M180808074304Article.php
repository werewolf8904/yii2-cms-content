<?php

namespace werewolf8904\cmscontent\migrations;

use werewolf8904\cmscore\db\Migration;

/**
 * Class M180808074304Article
 */
class M180808074304Article extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = $this->tableOptions;
        //article_category
        $this->createTable('{{%article_category}}', [
            'id' => $this->primaryKey(),
            'status' => $this->tinyInteger(1)->notNull()->defaultValue(0),
            'sort' => $this->integer()->notNull()->defaultValue(0),
            'parent_id' => $this->integer(),
            'view' => $this->string(255)->notNull(),
            'view_item' => $this->string(255)->notNull(),
            'slug' => $this->string(255)->notNull()->defaultValue(''),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        //article_category_i18n
        $this->createTable('{{%article_category_i18n}}', [
            'id' => $this->primaryKey(),
            'language_code' => $this->string(6)->notNull(),
            'article_category_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'content' => $this->text(),
            'seo_title' => $this->string(80)->notNull()->defaultValue(''),
            'seo_h1' => $this->string()->notNull()->defaultValue(''),
            'seo_description' => $this->string(180)->notNull()->defaultValue(''),
        ], $tableOptions);
        $this->createIndex('ac18n_lang_f_id', '{{%article_category_i18n}}', ['article_category_id', 'language_code',], true);
        $this->addForeignKey('FK_article_category_i18n_article_category',
            '{{%article_category_i18n}}', 'article_category_id',
            '{{%article_category}}', 'id',
            'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_article_category_i18n_language',
            '{{%article_category_i18n}}', 'language_code',
            '{{%language}}', 'code',
            'CASCADE', 'CASCADE');

        //article
        $this->createTable('{{%article}}', [
            'id' => $this->primaryKey(),
            'status' => $this->tinyInteger(1)->notNull()->defaultValue(0),
            'use_short_description' => $this->tinyInteger(1)->notNull()->defaultValue(0),
            'thumbnail' => $this->string(1024)->notNull()->defaultValue(''),
            'article_category_id' => $this->integer(),
            'slug' => $this->string(255)->notNull()->defaultValue(''),
            'view' => $this->string(255)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'published_at' => $this->integer()->notNull(),
            'end_at' => $this->integer(),
        ], $tableOptions);
        $this->addForeignKey('FK_article_2_article_category',
            '{{%article}}', 'article_category_id',
            '{{%article_category}}', 'id',
            'CASCADE', 'CASCADE');

        //article_i18n
        $this->createTable('{{%article_i18n}}', [
            'id' => $this->primaryKey(),
            'language_code' => $this->string(6)->notNull(),
            'article_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'content' => $this->text(),
            'short_description' => $this->text()->notNull(),
            'seo_title' => $this->string(80)->notNull()->defaultValue(''),
            'seo_h1' => $this->string()->notNull()->defaultValue(''),
            'seo_description' => $this->string(180)->notNull()->defaultValue(''),
        ], $tableOptions);
        $this->createIndex('a18n_lang_f_id', '{{%article_i18n}}', ['article_id', 'language_code',], true);
        $this->addForeignKey('fk_article_i18n_2_article',
            '{{%article_i18n}}', 'article_id',
            '{{%article}}', 'id',
            'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_article_i18n_2_language',
            '{{%article_i18n}}', 'language_code',
            '{{%language}}', 'code',
            'CASCADE', 'CASCADE');

        //article_attachment
        $this->createTable('{{%article_attachment}}', [
            'id' => $this->primaryKey(),
            'article_id' => $this->integer()->notNull(),
            'path' => $this->string()->notNull(),
            'base_url' => $this->string()->notNull(),
            'type' => $this->string()->notNull(),
            'size' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'order' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey(
            'fk_article_attachment_article',
            '{{%article_attachment}}',
            'article_id',
            '{{%article}}',
            'id',
            'cascade',
            'cascade'
        );

        //article_tags
        $this->createTable('{{%article_tags}}', [
            'article_id' => $this->integer()->notNull(),
            'tag_id' => $this->integer()->notNull()
        ], $tableOptions);
        $this->addPrimaryKey('atags', '{{%article_tags}}', ['article_id', 'tag_id',]);
        $this->addForeignKey(
            'fk_article_tags_2_article',
            '{{%article_tags}}',
            'article_id',
            '{{%article}}',
            'id',
            'cascade',
            'cascade'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        //article_tags
        $this->dropTable('{{%article_tags}}');

        //article_i18n
        $this->dropTable('{{%article_i18n}}');

        //article_attachment
        $this->dropForeignKey('fk_article_attachment_article', '{{%article_attachment}}');
        $this->dropTable('{{%article_attachment}}');

        //article
        $this->dropTable('{{%article}}');

        //article_category_i18n
        $this->dropTable('{{%article_category_i18n}}');

        //article_category
        $this->dropTable('{{%article_category}}');
    }
}
