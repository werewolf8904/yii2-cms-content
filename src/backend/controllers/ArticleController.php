<?php

namespace werewolf8904\cmscontent\backend\controllers;

use werewolf8904\cmscontent\backend\models\Article;
use werewolf8904\cmscontent\backend\models\search\ArticleSearch;
use werewolf8904\cmscore\controllers\BackendController;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends BackendController
{
    public $searchClass = ArticleSearch::class;
    public $class = Article::class;
}
