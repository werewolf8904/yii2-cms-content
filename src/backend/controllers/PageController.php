<?php

namespace werewolf8904\cmscontent\backend\controllers;

use werewolf8904\cmscontent\backend\models\Page;
use werewolf8904\cmscontent\backend\models\search\PageSearch;
use werewolf8904\cmscore\controllers\BackendController;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends BackendController
{
    public $searchClass = PageSearch::class;
    public $class = Page::class;
}
