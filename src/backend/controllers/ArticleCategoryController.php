<?php

namespace werewolf8904\cmscontent\backend\controllers;

use werewolf8904\cmscontent\backend\models\ArticleCategory;
use werewolf8904\cmscontent\backend\models\search\ArticleCategorySearch;
use werewolf8904\cmscore\controllers\BackendController;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleCategoryController extends BackendController
{
    public $searchClass = ArticleCategorySearch::class;
    public $class = ArticleCategory::class;
}
