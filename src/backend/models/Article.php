<?php

namespace werewolf8904\cmscontent\backend\models;

use trntv\filekit\behaviors\UploadBehavior;
use werewolf8904\cmscontent\backend\models\query\ArticleQuery;
use werewolf8904\cmscontent\models\ArticleI18n;
use werewolf8904\cmscore\behaviors\MultilingualBehavior;
use werewolf8904\cmscore\models\Language;
use werewolf8904\cmscore\traits\BackendTrait;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class Article
 *
 * @property string $name
 * @property string $content
 * @property string $seo_title
 * @property string $seo_h1
 * @property string $seo_description
 * @property string $short_description
 *
 * @package backend\models
 */
class Article extends \werewolf8904\cmscontent\models\Article
{
    use BackendTrait;

    /**
     * @var array
     */
    public $attachments;

    /**
     * @var
     */
    public $tag;

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleCategory()
    {
        return $this->hasOne(ArticleCategory::class, ['id' => 'article_category_id',]);
    }

    public static function find()
    {
        return (new ArticleQuery(static::class))->multilingual();
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['content', 'short_description',], 'string',],
                [['seo_description',], 'string', 'max' => 180,],
                [['seo_title',], 'string', 'max' => 80,],
                [['name', 'seo_h1',], 'string', 'max' => 255,],
                [['attachments',], 'safe',],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'name' => Yii::t('content/model_labels', 'Name'),
                'content' => Yii::t('content/model_labels', 'Content'),
                'seo_title' => Yii::t('content/model_labels', 'Seo title'),
                'seo_h1' => Yii::t('content/model_labels', 'Seo h1'),
                'seo_description' => Yii::t('content/model_labels', 'Seo description'),
                'short_description' => Yii::t('content/model_labels', 'Short description'),
            ]
        );
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $b = parent::behaviors();
        $b['ml'] = [
            'class' => MultilingualBehavior::class,
            'languages' => Language::getLanguages(),
            'languageField' => 'language_code',
            //'localizedPrefix' => '',
            'requireTranslations' => false,
            //'dynamicLangClass' => true,
            'langClassName' => ArticleI18n::class,
            'defaultLanguage' => \Yii::$app->language,
            'langForeignKey' => 'article_id',
            'tableName' => ArticleI18n::tableName(),
            'attributes' => [
                'name', 'content', 'seo_title', 'seo_h1', 'seo_description', 'short_description',
            ],
        ];
        $b['upload'] = [
            'class' => UploadBehavior::class,
            'attribute' => 'attachments',
            'multiple' => true,
            'pathAttribute' => 'path',
            'baseUrlAttribute' => 'base_url',
            'uploadRelation' => 'articleAttachments',
            'typeAttribute' => 'type',
            'sizeAttribute' => 'size',
            'nameAttribute' => 'name',
            'orderAttribute' => 'order',
        ];
        $b['slug'] = [
            'class' => SluggableBehavior::class,
            'attribute' => 'name',
            'immutable' => true,
            'ensureUnique' => true,
        ];
        return $b;
    }

    /**
     * @return string
     */
    public function getPathString()
    {
        return ($this->articleCategory ? $this->articleCategory->pathString . '/' : '') . $this->slug;
    }
}
