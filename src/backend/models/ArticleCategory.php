<?php

namespace werewolf8904\cmscontent\backend\models;

use werewolf8904\cmscontent\backend\models\query\ArticleCategoryQuery;
use werewolf8904\cmscontent\models\ArticleCategoryI18n;
use werewolf8904\cmscore\behaviors\MultilingualBehavior;
use werewolf8904\cmscore\models\Language;
use werewolf8904\cmscore\traits\BackendTrait;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class ArticleCategory
 *
 * @property string $name
 * @property string $content
 * @property string $seo_title
 * @property string $seo_h1
 * @property string $seo_description
 *
 * @package backend\models
 */
class ArticleCategory extends \werewolf8904\cmscontent\models\ArticleCategory
{
    use BackendTrait;

    /**
     * @return ArticleCategoryQuery
     */
    public static function find()
    {
        return (new ArticleCategoryQuery(static::class))->multilingual();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['content',], 'string',],
                [['seo_description',], 'string', 'max' => 180,],
                [['seo_title',], 'string', 'max' => 80,],
                [['name', 'seo_h1',], 'string', 'max' => 255,],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'name' => Yii::t('content/model_labels', 'Name'),
                'content' => Yii::t('content/model_labels', 'Content'),
                'seo_title' => Yii::t('content/model_labels', 'Seo Title'),
                'seo_h1' => Yii::t('content/model_labels', 'Seo H1'),
                'seo_description' => Yii::t('content/model_labels', 'Seo Description'),
            ]
        );
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $b = parent::behaviors();
        $b['ml'] = [
            'class' => MultilingualBehavior::class,
            'languages' => Language::getLanguages(),
            'languageField' => 'language_code',
            //'localizedPrefix' => '',
            'requireTranslations' => false,
            //'dynamicLangClass' => true,
            'langClassName' => ArticleCategoryI18n::class,
            'defaultLanguage' => \Yii::$app->language,
            'langForeignKey' => 'article_category_id',
            'tableName' => ArticleCategoryI18n::tableName(),
            'attributes' => [
                'name', 'content', 'seo_title', 'seo_h1', 'seo_description',
            ],
        ];
        $b['slug'] = [
            'class' => SluggableBehavior::class,
            'attribute' => 'name',
            'immutable' => true,
            'ensureUnique' => true,
        ];
        return $b;
    }

    /**
     * @return string
     */
    public function getNamePathString()
    {
        $arr[] = $this->name;
        $this->getParentsName($this->parent, $arr);
        return implode('/', $arr);
    }

    /**
     * @param $model
     * @param $arr
     */
    protected function getParentsName($model, &$arr): void
    {
        if ($model && isset($model['name'])) {
            $arr[] = $model->slug;
            if ($model->parent) {
                $this->getParentsName($model, $arr);
            }
        }
    }
}
