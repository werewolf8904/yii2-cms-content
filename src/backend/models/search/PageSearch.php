<?php

namespace werewolf8904\cmscontent\backend\models\search;

use werewolf8904\cmscontent\backend\models\Page;
use yii\data\ActiveDataProvider;

/**
 * PageSearch represents the model behind the search form about `common\models\Page`.
 */
class PageSearch extends Page
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'status',], 'integer',],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Page::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (isset($_GET['PageSearch']) && !($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
        ]);
        return $dataProvider;
    }
}
