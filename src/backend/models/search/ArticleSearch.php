<?php

namespace werewolf8904\cmscontent\backend\models\search;

use werewolf8904\cmscontent\backend\models\Article;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * ArticleSearch represents the model behind the search form about `common\models\Article`.
 */
class ArticleSearch extends Article
{
    const TYPE_CLASS = Article::class;

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'status', 'published_at', 'created_at', 'updated_at', 'article_category_id', 'tag',], 'integer',],
            [['slug',], 'safe',]
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param $params
     *
     * @return ActiveDataProvider
     * @throws \ReflectionException
     */
    public function search($params)
    {
        /**
         * @var $model_class \werewolf8904\cmscontent\backend\models\ArticleCategory
         */
        $model_class = static::TYPE_CLASS;
        $query = $model_class::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);
        $reflect = new \ReflectionClass($this);
        $kartik_name = $reflect->getShortName();
        if (Yii::$app->request->get($kartik_name) && !($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'tag_id' => $this->tag,
            'id' => $this->id,
            'status' => $this->status,
            'article_category_id' => $this->article_category_id,
            'published_at' => $this->published_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);
        return $dataProvider;
    }
}
