<?php

namespace werewolf8904\cmscontent\backend\models\query;

use werewolf8904\cmscore\traits\MultilingualTrait;

/**
 * Class PageQuery
 *
 * @package backend\models\query
 */
class PageQuery extends \werewolf8904\cmscontent\models\query\PageQuery
{
    use MultilingualTrait;
}
