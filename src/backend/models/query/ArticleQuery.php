<?php

namespace werewolf8904\cmscontent\backend\models\query;

use werewolf8904\cmscore\traits\MultilingualTrait;

/**
 * Class ArticleQuery
 *
 * @package backend\models\query
 */
class ArticleQuery extends \werewolf8904\cmscontent\models\query\ArticleQuery
{
    use MultilingualTrait;
}
