<?php

namespace werewolf8904\cmscontent\backend\models\query;

use werewolf8904\cmscore\traits\MultilingualTrait;

/**
 * Class ArticleQuery
 * @method multilingual
 *
 * @package backend\models\query
 */
class ArticleCategoryQuery extends \werewolf8904\cmscontent\models\query\ArticleCategoryQuery
{
    use MultilingualTrait;
}
