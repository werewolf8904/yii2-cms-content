<?php

namespace werewolf8904\cmscontent\backend\models;

use werewolf8904\cmscontent\backend\models\query\PageQuery;
use werewolf8904\cmscontent\models\PageI18n;
use werewolf8904\cmscore\behaviors\MultilingualBehavior;
use werewolf8904\cmscore\models\Language;
use werewolf8904\cmscore\traits\BackendTrait;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class Page
 *
 * @property string $name
 * @property string $content
 * @property string $seo_title
 * @property string $seo_h1
 * @property string $seo_description
 *
 * @package backend\models
 */
class Page extends \werewolf8904\cmscontent\models\Page
{
    use BackendTrait;

    /**
     * @inheritDoc
     */
    public static function find()
    {
        return (new PageQuery(static::class))->multilingual();
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        $b = parent::behaviors();
        $b['ml'] = [
            'class' => MultilingualBehavior::class,
            'languages' => Language::getLanguages(),
            'languageField' => 'language_code',
            //'localizedPrefix' => '',
            'requireTranslations' => false,
            //'dynamicLangClass' => true,
            'langClassName' => PageI18n::class, // or namespace/for/a/class/PostLang
            'defaultLanguage' => \Yii::$app->language,
            'langForeignKey' => 'page_id',
            'tableName' => PageI18n::tableName(),
            'attributes' => [
                'name', 'content', 'seo_title', 'seo_h1', 'seo_description',
            ],
        ];
        $b['slug'] = [
            'class' => SluggableBehavior::class,
            'attribute' => 'name',
            'immutable' => true,
            'ensureUnique' => true,
        ];
        return $b;
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['content',], 'string',],
                [['seo_description',], 'string', 'max' => 180,],
                [['seo_title',], 'string', 'max' => 80,],
                [['name', 'seo_h1',], 'string', 'max' => 255,],
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'name' => Yii::t('content/model_labels', 'Name'),
                'content' => Yii::t('content/model_labels', 'Content'),
                'seo_title' => Yii::t('content/model_labels', 'Seo Title'),
                'seo_h1' => Yii::t('content/model_labels', 'Seo H1'),
                'seo_description' => Yii::t('content/model_labels', 'Seo Description'),
            ]
        );
    }
}
