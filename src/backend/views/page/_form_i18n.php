<?php
/**
 * @var $this yii\web\View
 * @var $model \werewolf8904\cmscontent\models\Page
 * @var $form yii\bootstrap\ActiveForm
 * @var $language \werewolf8904\cmscore\models\Language
 */

use alexantr\ckeditor\CKEditor;

echo $form->field(
    $model,
    $model->defaultLanguage === $language->code ? 'name' : 'name_' . $language->code
)->label($model->getAttributeLabel('name'))->textInput();

echo $form->field(
    $model,
    $model->defaultLanguage === $language->code ? 'content' : 'content_' . $language->code
)->label($model->getAttributeLabel('content'))->widget(CKEditor::class);

echo $form->field(
    $model,
    $model->defaultLanguage === $language->code ? 'seo_title' : 'seo_title_' . $language->code
)->label($model->getAttributeLabel('seo_title'))->textInput();

echo $form->field(
    $model,
    $model->defaultLanguage === $language->code ? 'seo_h1' : 'seo_h1_' . $language->code
)->label($model->getAttributeLabel('seo_h1'))->textInput();

echo $form->field(
    $model,
    $model->defaultLanguage === $language->code ? 'seo_description' : 'seo_description_' . $language->code
)->label($model->getAttributeLabel('seo_description'))->textarea();
