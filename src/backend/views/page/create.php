<?php
/**
 * @var $this yii\web\View
 * @var $model \werewolf8904\cmscontent\backend\models\Page
 * @var $languages \werewolf8904\cmscore\models\Language[]
 */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => Yii::t('backend', 'Page'),
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Pages'), 'url' => ['index',],];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-create">
    <?= $this->render('_form', compact('model', 'languages')) ?>
</div>
