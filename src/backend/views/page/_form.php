<?php
/**
 * @var $this      yii\web\View
 * @var $model     \werewolf8904\cmscontent\backend\models\Page
 * @var $form      yii\bootstrap\ActiveForm
 * @var $languages \werewolf8904\cmscore\models\Language[]
 * */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Tabs;

?>
<div class="page-form">
    <?php
    $form = ActiveForm::begin();
    $items = [];
    foreach ($languages as $language) {
        $items[] = [
            'label' => $language->name,
            'content' => $this->render('_form_i18n', compact('form', 'model', 'language'))
        ];
    }
    ?>
    <?=  Tabs::widget([
        'items' => $items
    ]);
    ?>
    <hr>
    <?= $form->field($model, 'slug')
        ->hint(Yii::t('backend', 'If you\'ll leave this field empty, slug will be generated automatically'))
        ->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'view')
        ->dropDownList($model->getTemplates()) ?>
    <?= $form->field($model, 'status')->widget(\kartik\widgets\SwitchInput::class, [
        'type' => \kartik\widgets\SwitchInput::CHECKBOX
    ]); ?>
    <?php if (!$model->isNewRecord): ?>
        <div class="form-group field-album-created_at">
            <label class="control-label" for="album-name"><?= $model->getAttributeLabel('created_at'); ?></label>
            <br/>
            <?= Yii::$app->formatter->asDate($model->created_at) ?>
            <div class="help-block"></div>
        </div>
        <div class="form-group field-album-created_at">
            <label class="control-label" for="album-name"><?= $model->getAttributeLabel('updated_at'); ?></label>
            <br/>
            <?= Yii::$app->formatter->asDate($model->updated_at) ?>
            <div class="help-block"></div>
        </div>
    <?php endif; ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',]) ?>
        <?= Html::submitButton(Yii::t('backend', 'Save and stay'), ['class' => 'btn btn-success', 'name' => 'stay']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
