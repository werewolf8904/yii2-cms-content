<?php
/**
 * @var $this yii\web\View
 * @var $model \werewolf8904\cmscontent\backend\models\Page
 * @var $languages \werewolf8904\cmscore\models\Language[]
 */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
        'modelClass' => Yii::t('backend', 'Page'),
    ]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Pages'), 'url' => ['index',],];
$this->params['breadcrumbs'][] = ['label' => $model->name];
?>
<div class="page-update">
    <?= $this->render('_form', compact('model', 'languages')) ?>
</div>
