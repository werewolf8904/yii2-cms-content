<?php
/**
 * @var $this yii\web\View
 * @var $searchModel \werewolf8904\cmscontent\backend\models\search\PageSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 */

use kartik\editable\Editable;
use kartik\grid\EditableColumn;
use kartik\grid\GridView;

$columns = [
    [
        'attribute' => 'id',
        'headerOptions' => ['class' => 'id-column',],
    ],
    'name',
    [
        'class' => EditableColumn::class,
        'attribute' => 'status',
        'filter' => [
            Yii::t('backend', 'Not Published'),
            Yii::t('backend', 'Published'),
        ],
        'vAlign' => 'middle',
        'contentOptions' => ['style' => 'max-width: 50px;',],
        'editableOptions' => [
            'format' => Editable::FORMAT_BUTTON,
            'displayValueConfig' => ['0' => GridView::ICON_INACTIVE, '1' => GridView::ICON_ACTIVE,],
            'inputType' => \kartik\editable\Editable::INPUT_SWITCH,
            'placement' => 'auto',
            'options' => [
                'pluginOptions' => ['min' => 0, 'max' => 50000,],
            ],
        ],
    ],
]; ?>
<?= $this->render('@core/views/_common/index', [
    'columns' => $columns,
    'title' => Yii::t('backend', 'Pages'),
    'title_create' => Yii::t('backend', 'Page'),
    'dataProvider' => $dataProvider,
    'searchModel' => $searchModel,
]) ?>
