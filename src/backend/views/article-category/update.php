<?php
/**
 * @var $this yii\web\View
 * @var $model \werewolf8904\cmscontent\backend\models\ArticleCategory
 * @var $languages \werewolf8904\cmscore\models\Language[]
 */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
        'modelClass' => Yii::t('backend', 'Article Category'),
    ]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Article Categories'), 'url' => ['index',],];
$this->params['breadcrumbs'][] = ['label' => $model->name,];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="article-update">
    <?= $this->render('_form', compact('model', 'languages')) ?>
</div>
