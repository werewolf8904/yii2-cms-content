<?php
/**
 * @var $this yii\web\View
 * @var $model \werewolf8904\cmscontent\backend\models\ArticleCategory
 * @var $languages \werewolf8904\cmscore\models\Language[]
 */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => Yii::t('backend', 'Article Category'),
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Article Categories'), 'url' => ['index',],];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-create">
    <?= $this->render('_form', compact('model', 'languages')) ?>
</div>
