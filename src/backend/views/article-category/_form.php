<?php
/**
 * @var $this      yii\web\View
 * @var $model     \werewolf8904\cmscontent\backend\models\ArticleCategory
 * @var $form      yii\bootstrap\ActiveForm
 * @var $languages \werewolf8904\cmscore\models\Language[]
 * */

use werewolf8904\cmscontent\backend\models\ArticleCategory;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<div class="article-form">
    <?php
    $form = ActiveForm::begin();
    $items = [];
    foreach ($languages as $language) {
        $items[] = [
            'label' => $language->name,
            'content' => $this->render('_form_i18n', compact('form', 'model', 'language')),
        ];
    }
    ?>
    <?= \yii\bootstrap\Tabs::widget([
        'items' => $items,
    ]);
    ?>
    <?= $form->field($model, 'parent_id')->widget(
        \kartik\select2\Select2::class,
        [
            'data' => \yii\helpers\ArrayHelper::map(ArticleCategory::find()->andWhere(['!=', 'id', $model->isNewRecord ? 0 : $model->id,])->all(), 'id', 'namePathString'),
            'options' => [
                'placeholder' => '',
                'prompt' => '-',
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]
    ); ?>
    <hr>
    <?= $form->field($model, 'sort')->textInput(); ?>
    <?= $form->field($model, 'view')->dropDownList($model->getTemplates()) ?>
    <?= $form->field($model, 'view_item')->dropDownList($model->getTemplates('tmpl_path_item')) ?>
    <?= $form->field($model, 'slug')
        ->hint(Yii::t('backend', 'If you\'ll leave this field empty, slug will be generated automatically'))
        ->textInput(['maxlength' => true]);
    ?>
    <?= $form->field($model, 'status')->widget(\kartik\widgets\SwitchInput::class, [
        'type' => \kartik\widgets\SwitchInput::CHECKBOX,
    ]); ?>
    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',]) ?>
        <?= Html::submitButton(Yii::t('backend', 'Save and stay'), ['class' => 'btn btn-success', 'name' => 'stay',]) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
