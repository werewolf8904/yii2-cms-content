<?php
/**
 * @var $this yii\web\View
 * @var $searchModel \werewolf8904\cmscontent\backend\models\search\ArticleSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 */

use kartik\editable\Editable;
use kartik\grid\EditableColumn;
use kartik\grid\GridView;

$columns = [
    [
        'attribute' => 'id',
        'headerOptions' => ['class' => 'id-column',],
    ],
    'name',
    [
        'class' => EditableColumn::class,
        'attribute' => 'status',
        'filter' => [
            Yii::t('backend', 'Not Published'),
            Yii::t('backend', 'Published'),
        ],
        'vAlign' => 'middle',
        'editableOptions' => [
            'format' => Editable::FORMAT_BUTTON,
            'displayValueConfig' => ['0' => GridView::ICON_INACTIVE, '1' => GridView::ICON_ACTIVE,],
            'inputType' => \kartik\editable\Editable::INPUT_SWITCH,
            'placement' => 'auto',
        ],
    ],
    'created_at:datetime',
]; ?>
<?= $this->render('@core/views/_common/index', [
    'columns' => $columns,
    'title' => Yii::t('backend', 'Article Categories'),
    'title_create' => Yii::t('backend', 'Article Category'),
    'dataProvider' => $dataProvider,
    'searchModel' => $searchModel,
]) ?>
