<?php

use alexantr\ckeditor\CKEditor;

/**
 * @var $this yii\web\View
 * @var $model \werewolf8904\cmscontent\backend\models\Article
 * @var $form yii\bootstrap\ActiveForm
 * @var $language \werewolf8904\cmscore\models\Language
 */

echo $form->field(
    $model,
    $model->defaultLanguage === $language->code ? 'name' : 'name_' . $language->code
)->label($model->getAttributeLabel('name'))->textInput();

echo $form->field(
    $model,
    $model->defaultLanguage === $language->code ? 'short_description' : 'short_description_' . $language->code
)->label($model->getAttributeLabel('short_description'))->textarea();

echo $form->field(
    $model,
    $model->defaultLanguage === $language->code ? 'content' : 'content_' . $language->code
)->label($model->getAttributeLabel('content'))->widget(CKEditor::class);

echo $form->field(
    $model,
    $model->defaultLanguage === $language->code ? 'seo_title' : 'seo_title_' . $language->code
)->label($model->getAttributeLabel('seo_title'))->textInput();

echo $form->field(
    $model,
    $model->defaultLanguage === $language->code ? 'seo_h1' : 'seo_h1_' . $language->code
)->label($model->getAttributeLabel('seo_h1'))->textInput();

echo $form->field(
    $model,
    $model->defaultLanguage === $language->code ? 'seo_description' : 'seo_description_' . $language->code
)->label($model->getAttributeLabel('seo_description'))->textarea();
