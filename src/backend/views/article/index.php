<?php
/**
 * @var $this yii\web\View
 * @var $searchModel \werewolf8904\cmscontent\backend\models\search\ArticleSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 */

use kartik\editable\Editable;
use kartik\grid\EditableColumn;
use kartik\grid\GridView;
use code2magic\glide\components\IGlide;
use yii\helpers\Html;

$data = \yii\helpers\ArrayHelper::map(\werewolf8904\cmscontent\backend\models\ArticleCategory::find()->all(), 'id', 'name');
$columns = [
    [
        'attribute' => 'id',
        'headerOptions' => ['class' => 'id-column',],
    ],
    'name',
    [
        'attribute' => 'article_category_id',
        'value' => 'articleCategory.name',
        'filter' => \kartik\widgets\Select2::widget([
            'model' => $searchModel,
            'pjaxContainerId' => 'product-pjax',
            'attribute' => 'article_category_id',
            'data' => $data,
            'options' => [
                'placeholder' => '',
                'prompt' => '-',
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ])
    ],
    [
        'class' => EditableColumn::class,
        'attribute' => 'thumbnail',
        'format' => 'raw',
        'value' => function ($model) {
            return $model->thumbnail ? Html::img(Yii::$container->get(IGlide::class)->getImageSourceUrl($model->thumbnail), ['style' => 'width: 40%',]) : null;
        },
        'vAlign' => 'middle',
        'editableOptions' => [
            'format' => Editable::FORMAT_BUTTON,
            'inputType' => Editable::INPUT_WIDGET,
            'widgetClass' => \alexantr\elfinder\InputFile::class,
            'placement' => 'auto',
        ],
    ],
    [

        'class' => EditableColumn::class,
        'attribute' => 'status',
        'filter' => [
            Yii::t('backend', 'Not Published'),
            Yii::t('backend', 'Published'),
        ],
        'vAlign' => 'middle',
        'contentOptions' => ['style' => 'max-width: 50px;',],
        'editableOptions' => [
            'format' => Editable::FORMAT_BUTTON,
            'displayValueConfig' => ['0' => GridView::ICON_INACTIVE, '1' => GridView::ICON_ACTIVE,],
            'inputType' => \kartik\editable\Editable::INPUT_SWITCH,
            'placement' => 'auto',
        ],
    ],
    'published_at:datetime',
    'created_at:datetime',
]; ?>
<?= $this->render('@core/views/_common/index', [
    'columns' => $columns,
    'title' => Yii::t('backend', 'Articles'),
    'title_create' => Yii::t('backend', 'Article'),
    'dataProvider' => $dataProvider,
    'searchModel' => $searchModel,
]) ?>
