<?php
/**
 * @var $this      yii\web\View
 * @var $model     \werewolf8904\cmscontent\backend\models\Article
 * @var $form      yii\bootstrap\ActiveForm
 * @var $languages \werewolf8904\cmscore\models\Language[]
 * */

use trntv\yii\datetime\DateTimeWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<div class="article-form">
    <?php
    $form = ActiveForm::begin();
    $items = [];
    foreach ($languages as $language) {
        $items[] = [
            'label' => $language->name,
            'content' => $this->render('_form_i18n', compact('form', 'model', 'language')),
        ];
    } ?>
    <?= \yii\bootstrap\Tabs::widget([
        'items' => $items,
    ]); ?>
    <hr>
    <?= $form->field($model, 'article_category_id')->widget(
        \kartik\select2\Select2::class,
        [
            'data' => \yii\helpers\ArrayHelper::map(\werewolf8904\cmscontent\backend\models\ArticleCategory::find()->all(), 'id', 'namePathString'),
            'options' => [
                'placeholder' => '',
                'prompt' => '-',
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]
    ); ?>
    <?= $form->field($model, 'view')->dropDownList($model->getTemplates()) ?>
    <?= $form->field($model, 'slug')
        ->hint(Yii::t('backend', 'If you\'ll leave this field empty, slug will be generated automatically'))
        ->textInput(['maxlength' => true,]) ?>
    <?= $form->field($model, 'thumbnail')->widget(\alexantr\elfinder\InputFile::class, [
        'clientRoute' => '//file/manager/input',
        'options' => [
            'class' => 'form-control yii2-elfinder-input input_preview',
            'data' => [
                'default-src' => '/no_image.png',
                'storage-src' => Yii::getAlias('@storageUrl/source')
            ],
        ],
    ]); ?>
    <?= $form->field($model, 'attachments')->widget(
        \trntv\filekit\widget\Upload::class,
        [
            'sortable' => true,
            'maxNumberOfFiles' => 10,
        ]);
    ?>
    <?= $form->field($model, 'status')->checkbox(); ?>
    <?= $form->field($model, 'use_short_description')->checkbox(); ?>
    <?= $form->field($model, 'published_at')->widget(
        DateTimeWidget::class,
        [
            'phpDatetimeFormat' => 'yyyy-MM-dd\'T\'HH:mm:ssZZZZZ',
        ]
    ) ?>
    <?= $form->field($model, 'end_at')->widget(
        DateTimeWidget::class,
        [
            'phpDatetimeFormat' => 'yyyy-MM-dd\'T\'HH:mm:ssZZZZZ',
        ]
    ) ?>
    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',]) ?>
        <?= Html::submitButton(Yii::t('backend', 'Save and stay'), ['class' => 'btn btn-success', 'name' => 'stay',]) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
