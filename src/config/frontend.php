<?php
return [
    'modules' => [
        'content' => [
            'class' => \werewolf8904\cmscontent\frontend\Module::class,
        ],
    ],
    'container' => [
        'singletons' => [
            \werewolf8904\urlfactory\IObjectUrlFactory::class => [
                'class' => \werewolf8904\urlfactory\ObjectUrlFactory::class,
                'map' => [
                    // Article
                    \werewolf8904\cmscontent\models\Article::class => ['/content/article/view', 'id',],
                    \werewolf8904\cmscontent\frontend\models\Article::class => ['/content/article/view', 'id',],
                    // ArticleCategory
                    \werewolf8904\cmscontent\models\ArticleCategory::class => ['/content/article/index', 'id',],
                    \werewolf8904\cmscontent\frontend\models\ArticleCategory::class => ['/content/article/index', 'id',],
                    // Page
                    \werewolf8904\cmscontent\models\Page::class => ['/content/page/view', 'id',],
                    \werewolf8904\cmscontent\frontend\models\Page::class => ['/content/page/view', 'id',],
                ],
            ],
        ]
    ],
];
