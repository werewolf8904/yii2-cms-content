<?php

namespace werewolf8904\cmscontent\frontend\models\query;

use werewolf8904\cmscore\traits\WithCurrentTranslation;

/**
 * 
 */
class PageQuery extends \werewolf8904\cmscontent\models\query\PageQuery
{
    protected $_relation = 'pageI18n';
    use WithCurrentTranslation;
}
