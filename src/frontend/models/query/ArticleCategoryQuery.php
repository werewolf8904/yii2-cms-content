<?php

namespace werewolf8904\cmscontent\frontend\models\query;

use werewolf8904\cmscore\traits\WithCurrentTranslation;

/**
 * 
 */
class ArticleCategoryQuery extends \werewolf8904\cmscontent\models\query\ArticleCategoryQuery
{
    protected $_relation = 'articleCategoryI18n';
    use WithCurrentTranslation;
}
