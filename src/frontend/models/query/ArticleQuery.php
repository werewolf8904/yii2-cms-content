<?php

namespace werewolf8904\cmscontent\frontend\models\query;

use werewolf8904\cmscore\traits\WithCurrentTranslation;

/**
 * 
 */
class ArticleQuery extends \werewolf8904\cmscontent\models\query\ArticleQuery
{
    protected $_relation = 'articleI18n';
    use WithCurrentTranslation;
}
