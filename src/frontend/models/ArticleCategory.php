<?php

namespace werewolf8904\cmscontent\frontend\models;

use werewolf8904\cmscontent\frontend\models\query\ArticleCategoryQuery;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class ArticleCategory
 *
 * @package frontend\models
 */
class ArticleCategory extends \werewolf8904\cmscontent\models\ArticleCategory
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $content;

    /**
     * @var string
     */
    public $seo_title;

    /**
     * @var string
     */
    public $seo_h1;

    /**
     * @var string
     */
    public $seo_description;

    /**
     * @var string
     */
    public $short_description;

    /**
     * @var string
     */
    public $seo_keywords;

    /**
     * @return \werewolf8904\cmscontent\frontend\models\query\ArticleCategoryQuery
     */
    public static function find()
    {
        return (new ArticleCategoryQuery(static::class))->published()->joinWithCurrentTranslation();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['content',], 'string',],
                [['seo_description',], 'string', 'max' => 180,],
                [['seo_title',], 'string', 'max' => 80,],
                [['name', 'seo_h1',], 'string', 'max' => 255,],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'name' => Yii::t('content/model_labels', 'Name'),
                'content' => Yii::t('content/model_labels', 'Content'),
                'seo_title' => Yii::t('content/model_labels', 'Seo Title'),
                'seo_h1' => Yii::t('content/model_labels', 'Seo H1'),
                'seo_description' => Yii::t('content/model_labels', 'Seo Description'),
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::class, ['category_id' => 'id',]);
    }

    /**
     * @param bool $scheme
     * @return mixed
     */
    public function getUrl($scheme = false)
    {
        return \werewolf8904\cmscore\helpers\Url::getObjectUrl($this, $scheme);
    }
}
