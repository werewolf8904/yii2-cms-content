<?php

namespace werewolf8904\cmscontent\frontend\models;

use werewolf8904\cmscontent\frontend\models\query\ArticleQuery;
use werewolf8904\cmscore\behaviors\GlideThumbnailBehavior;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class Article
 *
 * @package frontend\models
 */
class Article extends \werewolf8904\cmscontent\models\Article
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $content;

    /**
     * @var string
     */
    public $seo_title;

    /**
     * @var string
     */
    public $seo_h1;

    /**
     * @var string
     */
    public $seo_description;
    
    /**
     * @var string
     */
    public $short_description;

    /**
     * @return ArticleQuery
     */
    public static function find()
    {
        return (new ArticleQuery(static::class))->joinWithCurrentTranslation();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'thumbnail' => [
                'class' => GlideThumbnailBehavior::class,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['content', 'short_description',], 'string',],
                [['seo_description',], 'string', 'max' => 180,],
                [['seo_title',], 'string', 'max' => 80,],
                [['name', 'seo_h1',], 'string', 'max' => 255,],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'name' => Yii::t('content/model_labels', 'Name'),
                'content' => Yii::t('content/model_labels', 'Content'),
                'seo_title' => Yii::t('content/model_labels', 'Seo title'),
                'seo_h1' => Yii::t('content/model_labels', 'Seo h1'),
                'seo_description' => Yii::t('content/model_labels', 'Seo description'),
                'short_description' => Yii::t('content/model_labels', 'Short description'),
            ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleCategory()
    {
        return $this->hasOne(ArticleCategory::class, ['id' => 'article_category_id',]);
    }

    /**
     * @return mixed
     */
    public function getShort($length = 350)
    {
        return (
            $this->use_short_description || $this->short_description)
            ? $this->short_description
            : \yii\helpers\StringHelper::truncate(strip_tags($this->content), $length, '', null, true);
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getItemView()
    {
        $category = $this->articleCategory ?: \Yii::createObject(ArticleCategory::class);
        return $category->getItemView();
    }

    /**
     * @param bool $scheme
     * @return mixed
     */
    public function getUrl($scheme = false)
    {
        return \werewolf8904\cmscore\helpers\Url::getObjectUrl($this, $scheme);
    }
}
