<?php

namespace werewolf8904\cmscontent\frontend\models\search;

use werewolf8904\cmscontent\frontend\models\Article;
use werewolf8904\cmscontent\frontend\models\ArticleCategory;
use yii\data\ActiveDataProvider;
use Yii;

/**
 * ArticleSearch represents the model behind the search form about `common\models\Article`.
 */
class ArticleSearch extends Article
{
    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getMainView()
    {
        $category = Yii::createObject(ArticleCategory::class);
        return $category->getMainView();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_category_id',], 'integer',]
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param $params
     *
     * @return ActiveDataProvider
     * @throws \yii\base\InvalidArgumentException
     */
    public function search($params): ActiveDataProvider
    {
        $query = Article::find()
            ->with(['articleAttachments',])
            ->published();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!($this->load($params, '') && $this->validate())) {
            return $dataProvider;
        }
        $query->andWhere([
            static::tableName() . '.article_category_id' => $this->article_category_id,
        ]);
        return $dataProvider;
    }
}
