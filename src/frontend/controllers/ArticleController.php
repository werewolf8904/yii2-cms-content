<?php

namespace werewolf8904\cmscontent\frontend\controllers;

use werewolf8904\cmscontent\frontend\models\Article;
use werewolf8904\cmscontent\frontend\models\ArticleCategory;
use werewolf8904\cmscontent\frontend\models\search\ArticleSearch;
use werewolf8904\cmscore\controllers\FrontendController;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * @author Eugene Terentev <eugene@terentev.net>
 */
class ArticleController extends FrontendController
{
    public $pageSize = 15;

    /**
     * @param null $id
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex($id=null): string
    {
        $article_category = ArticleCategory::find()->andFilterWhere([ArticleCategory::tableName() . '.id' => $id,])->one();
        $params = \Yii::$app->request->queryParams;
        $params = $article_category ? ArrayHelper::merge($params, ['article_category_id' => $article_category->id,]) : $params;
        $searchModel = Yii::createObject(ArticleSearch::class);
        $model = $article_category ?: $searchModel;
        $dataProvider = $searchModel->search($params);
        $dataProvider->pagination->defaultPageSize = $this->pageSize;
        if (!$searchModel) {
            throw new NotFoundHttpException();
        }
        $this->getView()->title = $model->seo_title ?: $model->name;
        $this->getView()->registerMetaTag(['name' => 'description','content' => $model->seo_description,],'description');
        $this->getView()->params['breadcrumbs'][] = $model->name;
        $dataProvider->sort = [
            'defaultOrder' => ['published_at' => SORT_DESC]
        ];
        return $this->render($model->getMainView(), ['dataProvider' => $dataProvider, 'model' => $model]);
    }

    /**
     * @param $id
     *
     * @return string
     * @throws \yii\base\InvalidArgumentException
     * @throws NotFoundHttpException
     */
    public function actionView($id): string
    {
        /**
         * @var $model Article
         */
        $model = Article::find()->andWhere([Article::tableName() . '.id' => $id])->one();
        if (!$model) {
            throw new NotFoundHttpException;
        }
        $this->getView()->title = $model->seo_title ?: $model->name;
        if($model->articleCategory) {
            $this->getView()->params['breadcrumbs'][] = [
                'label' => $model->articleCategory->name,
                'url' => \werewolf8904\cmscore\helpers\Url::getObjectUrl($model->articleCategory),
            ];
        }
        $this->getView()->registerMetaTag(['name' => 'description','content' => $model->seo_description,],'description');
        $this->getView()->params['breadcrumbs'][] = $model->name;
        return $this->render($model->getViewPath(), ['model' => $model,]);
    }
}
