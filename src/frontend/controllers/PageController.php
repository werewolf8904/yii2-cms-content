<?php

namespace werewolf8904\cmscontent\frontend\controllers;

use werewolf8904\cmscontent\frontend\models\Page;
use werewolf8904\cmscore\controllers\FrontendController;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class PageController
 *
 * @package frontend\controllers
 */
class PageController extends FrontendController
{
    /**
     * @param $id
     *
     * @return string
     * @throws \yii\base\InvalidArgumentException
     * @throws NotFoundHttpException
     */
    public function actionView($id): ?string
    {
        $model = Page::find()->andWhere([Page::tableName() . '.id' => $id,])->published()->one();
        if (!$model) {
            throw new NotFoundHttpException(Yii::t('frontend', 'Page not found'));
        }
        $viewFile = $model->getViewPath() ?: 'view';
        if (Yii::$app->request->isAjax) {
            return $this->renderPartial($viewFile, ['model' => $model,]);
        }
        $this->getView()->title = $model->seo_title ?: $model->name;
        $this->getView()->registerMetaTag(['name' => 'description','content' => $model->seo_description,],'description');
        $this->getView()->params['breadcrumbs'][] = $model->name;
        return $this->render($viewFile, ['model' => $model,]);
    }
}
