<?php
/**
 * @tmplVar label Default
 * @tmplVar order 1
 *
 * @var $this         yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $model        \werewolf8904\cmscontent\frontend\models\ArticleCategory
 */

?>
<!-- Begin Article -->
<div class="container">
    <div class="row">
        <div class="col-lg-12 сol-md-12 col-sm-12 col-xs-12 ">
            <span class="item-wrap-title"><?= $model->seo_h1 ?: $model->name; ?></span>
        </div>
    </div>
    <?php foreach ($dataProvider->models as $item) {
        echo $this->render($model->getItemView(), ['model' => $item]);
    } ?>
    <div class="row">
        <div class="paginator col-lg-12 сol-md-12 col-sm-12 col-xs-12">
            <?= \yii\widgets\LinkPager::widget([
                'pagination' => $dataProvider->pagination,
            ]) ?>
        </div>
    </div>
</div>
<!-- End Article -->
