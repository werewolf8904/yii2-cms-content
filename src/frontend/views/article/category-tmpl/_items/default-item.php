<?php
/**
 * @tmplVar label Default item
 * @tmplVar order 1
 *
 * @var $this  yii\web\View
 * @var $model \werewolf8904\cmscontent\frontend\models\Article
 */
?>
<!-- Begin Articles item -->
<div class="row">
    <div class="col-md-3 col-lg-3">
        <img src="<?= $model->getGlideThumbnail() ?>" alt="">
    </div>
    <div class="col-md-9 col-lg-9">
        <a href="<?= $model->getUrl() ?>">
            <?= $model->name ?>
        </a>
        <div>
            <?= $model->getShort() ?>
        </div>
    </div>
</div>
<!-- End Articles item -->
