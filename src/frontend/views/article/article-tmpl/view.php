<?php
/**
 * @tmplVar label Default
 * @tmplVar order 1
 *
 * @var $this yii\web\View
 * @var $model \werewolf8904\cmscontent\frontend\models\Article
 */
?>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <?php if ($model->thumbnail) { ?>
                        <div>
                            <img src="<?= $model->glideThumbnail ?>" alt="" class="display-inline">
                        </div>
                    <?php } ?>
                    <h2 class="display-inline"><?= $model->seo_h1 ?: $model->name ?></h2>
                    <span><?= $model->getPublishedDate() ?></span>
                </div>
            </div>
            <div class="row">
                <div class="wysiwyg">
                    <?= $model->content ?>
                </div>
                <?php if (!empty($model->articleAttachments)): ?>
                    <h3><?= Yii::t('frontend', 'Attachments') ?></h3>
                    <ul id="article-attachments">
                        <?php foreach ($model->articleAttachments as $attachment): ?>
                            <li>
                                <?= \yii\helpers\Html::a($attachment->name, $attachment->url) ?>
                                - <?= Yii::$app->formatter->asSize($attachment->size) ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
