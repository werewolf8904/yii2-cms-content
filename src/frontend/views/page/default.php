<?php
/**
 * @tmplVar label Default item
 * @tmplVar order 1
 *
 * @var $this  \yii\web\View
 * @var $model \werewolf8904\cmscontent\frontend\models\Page
 */
?>
<div class="container">
    <div class="row">
        <div class="about-text col-lg-12 сol-md-12 col-sm-12 col-xs-12">
            <div class="iw-header">
                <h1 class="item-wrap-title"><?= $model->seo_h1 ?: $model->name ?></h1>
            </div>
            <div class="text-wrap">
                <?= $model->content ?>
                <div class="clear-fix"></div>
            </div>
        </div>
    </div>
</div>
