<?php

namespace werewolf8904\cmscontent\widgets\articles;

use werewolf8904\cmscontent\backend\models\query\ArticleQuery;
use werewolf8904\cmscontent\models\Article;
use werewolf8904\cmscontent\models\ArticleCategory;
use Yii;
use yii\base\Widget;
use yii\behaviors\CacheableWidgetBehavior;
use yii\caching\TagDependency;

/**
 * This is the widget to display articles
 *
 * @property integer $id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 *
 */
class ArticlesWidget extends Widget
{
    /**
     * @var string text block key
     */
    public $count = 0;

    /**
     * @var array|string
     */
    public $article_category_id;

    /**
     * @var array|string
     */
    public $exclude;

    /**
     * @var string
     * @deprecated Not recommended to use, use view_wrap instead
     */
    public $view_item;

    /**
     * @var string
     */
    public $view_wrap;

    /**
     * @var array additional view params key=>value
     */
    public $view_params = [];

    /**
     * @var string
     */
    protected $_model_class = \werewolf8904\cmscontent\frontend\models\Article::class;

    public function behaviors()
    {
        return [
            [
                'class' => CacheableWidgetBehavior::class,
                'cacheDuration' => 0,
                'cacheKeyVariations' => [
                    Yii::$app->language,
                    $this->article_category_id,
                    $this->view_item,
                    $this->view_params,
                    $this->count
                ],
                'cacheDependency' => [
                    'class' => TagDependency::class,
                    'tags' => [
                        Article::class,
                        ArticleCategory::class
                    ],
                ],
            ],
        ];
    }

    public function run()
    {
        /** @var ArticleQuery $query */
        $query = $this->_model_class::find()->published()->joinWithCurrentTranslation();
        if ($this->exclude) {
            $query->andFilterWhere(['not in', $this->_model_class::tableName() . '.id', $this->exclude]);
        }
        if ($this->article_category_id) {
            $query->in_categories(['article_category_id' => $this->article_category_id]);
        }
        if ($this->count) {
            $query
                ->limit($this->count);
        }
        $models = $query
            ->orderBy(['published_at' => SORT_DESC])
            ->indexBy('id')
            ->all();
        if (\count($models) < 1) {
            return '';
        }
        $view = $this->view_wrap ?? $this->view_item ?? 'index';
        $view_params = array_merge($this->view_params, ['models' => $models]);
        return $this->render($view, $view_params);
    }
}
