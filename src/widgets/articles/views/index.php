<?php
/**
 * Created by PhpStorm.
 * User: werewolf
 * Date: 15.08.2018
 * Time: 9:50
 */
/**
 * @var $models \werewolf8904\cmscontent\frontend\models\Article[]
 * @var $this   \yii\web\View
 */
?>
<?php foreach ($models as $model): ?>
    <div class="row padding-md padding-lg">
        <?= $this->render('_item', compact('model')); ?>
    </div>
<?php endforeach; ?>
