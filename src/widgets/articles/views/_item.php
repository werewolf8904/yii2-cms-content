<?php
/**
 * Created by PhpStorm.
 * User: werewolf
 * Date: 15.08.2018
 * Time: 11:05
 */
/**
 * @var $model \werewolf8904\cmscontent\frontend\models\Article
 */
?>
<div class="col-md-6 col-lg-6">
    <img src="<?= $model->getGlideThumbnail() ?>">
</div>
<div class="col-md-6 col-lg-6">
    <a href="<?= \werewolf8904\cmscore\helpers\Url::getObjectUrl($model) ?>">
        <span><?= $model->name ?></span>
    </a>
    <div><?= $model->getShort(100) ?></div>
</div>
